# Important
## Devolepment has stopped on this project. It has continued in afk-minecraft 
https://gitlab.com/RincewindLangner/afk-minecraft 

# Minecraft mouse wheel spinner

### Used for placing down random blocks from your item bar.

This will spin your mouse wheel to allow you to place random blocks from your item bar with the right mouse button.

## Intended program for use with

This mini program is intended to be used with the Minecraft Java version but could work with other version or programs as well.

## Requirements

Built using:
 
*Python3.8*

*pynput 1.6.7*

Built for Linux and runs in the terminal window but my work on other platforms. 